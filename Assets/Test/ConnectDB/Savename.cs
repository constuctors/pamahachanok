﻿using UnityEngine;
using System.Collections;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Savename : MonoBehaviour {
    private string conn;
    public  Text NamePlayer;
    
	
	void Start () {
        conn = "URI=file:" + Application.dataPath + "/Test/DbGame.db";
        //ShowData();
        //InsertName("chai");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    
    private void ShowData()
    {
        IDbConnection dbcon = new SqliteConnection(conn);
        IDbCommand dbcmd = dbcon.CreateCommand();
        dbcon.Open();

        string sqlQuery = "SELECT name_use FROM User";
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            string name = reader.GetString(0);
            //Debug.Log("name = " + name);
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbcon.Close();
        dbcon = null;
    }
    

    public void InputName()
    {
        if(NamePlayer.text != string.Empty)
        {
            InsertName(NamePlayer.text);
            PlayerPrefs.SetString("Nameplay", NamePlayer.text);
            NamePlayer.text = string.Empty;
            //ShowData();
            SceneManager.LoadScene(9);
            
        }
    }


    private void InsertName(string name)
    {
        using (IDbConnection dbConnection = new SqliteConnection(conn))
        {
            dbConnection.Open();
            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                string sqlQuery = string.Format("INSERT INTO User (name_use) VALUES (\"{0}\")", name);

                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
                dbConnection.Close();
            }
        }
    }
}
