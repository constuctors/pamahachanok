﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class NameScript : MonoBehaviour {

    public GameObject NameUse;
    public GameObject date;

    public void SetName(int id,string name,string date)
    {
        this.NameUse.GetComponent<Text>().text = name;
        this.date.GetComponent<Text>().text = date;
    }
}
