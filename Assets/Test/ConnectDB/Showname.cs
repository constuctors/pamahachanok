﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Showname : MonoBehaviour {

    private string conn;
    //public Text ShowName;
    public GameObject NamePref;
    private int IduserS;
    public int idget;
    public Transform nameParent;
    public Button id;

    private List<classUser> showname = new List<classUser>();

	// Use this for initialization
	void Start () {
        conn = "URI=file:" + Application.dataPath + "/Test/DbGame.db";
        //ShowData();
        
        ShowNames();
        idget = IduserS;
        Debug.Log(idget);
        //InsertName("mild");
        //ShowName.text = PlayerPrefs.GetString("Nameplay");
    }
     void Update()
    {
        
        
    }

    private void ShowData()
    {
        showname.Clear();

        IDbConnection dbcon = new SqliteConnection(conn);
        IDbCommand dbcmd = dbcon.CreateCommand();
        dbcon.Open();

        string sqlQuery = "SELECT * FROM User";
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            showname.Add(new classUser(reader.GetInt32(0),reader.GetString(1), reader.GetDateTime(2)));
            
            //string name = reader.GetString(0);
            //Debug.Log(sqlQuery);
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbcon.Close();
        dbcon = null;
    }

    public void ShowNames()
    {
        ShowData();
        for (int i = 0; i < showname.Count; i++)
        {
            GameObject tmpOjc = Instantiate(NamePref);
            classUser tmpName = showname[i];

            tmpOjc.GetComponent<NameScript>().SetName(tmpName.idUser,tmpName.nameUser,tmpName.dateAdd.ToString());
            IduserS = tmpName.idUser;
            Button iduse = id.GetComponent<Button>();
            iduse.onClick.AddListener(ShowIdUser);
            //ShowIdUser(IduserS);
            tmpOjc.transform.SetParent(nameParent);
        }
    }
    public void ShowIdUser()
    {
        int idu = IduserS;
        Debug.Log(idu);
    }
}
