﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

class classUser
{
    public int idUser { get; set; }
    public string nameUser { get; set; }
    public DateTime dateAdd { get; set; }

    public classUser(int id,string name , DateTime date)
    {
        this.idUser = id;
        this.nameUser = name;
        this.dateAdd = date;
    }
}