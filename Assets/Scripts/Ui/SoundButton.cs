﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class SoundButton : MonoBehaviour {

    public AudioClip sound;
    public int i = 0;

    private Button bt { get { return GetComponent<Button>(); } }
    private AudioSource source { get { return GetComponent<AudioSource>(); } }

	// Use this for initialization
	void Start () {

        gameObject.AddComponent<AudioSource>();
        source.clip = sound;
        source.playOnAwake = false;

        bt.onClick.AddListener(() => PlaySound());
	}
	
	// Update is called once per frame
	void PlaySound()
    {
        source.PlayOneShot(sound);
    }

    public void SkipVideo(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
        i = 2;
    }
}
