﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiteStart : MonoBehaviour
{
    public GameObject panels1;

    void Start()
    {
        Time.timeScale = 0f;
        panels1.SetActive(true);
    }

    public void SetActiveP2()
    {
        panels1.SetActive(false);
        Time.timeScale = 1f;

    }
}