﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Uisprite : MonoBehaviour
{
    public GameObject panels1;
    public GameObject panels2;
    public GameObject panels3;
    public GameObject panels4;




    void Start()
    {
        
        Time.timeScale = 0f;
        panels1.SetActive(true);
        panels2.SetActive(false);
        panels3.SetActive(false);
        panels4.SetActive(false);



    }

    void Update()
    {
        
    }

    public void SetActiveP2()
    {
        panels1.SetActive(false);
        panels2.SetActive(true);
        panels3.SetActive(false);
        panels4.SetActive(false);

    }
    public void SetActiveP3()
    {
        panels1.SetActive(false);
        panels2.SetActive(false);
        panels3.SetActive(true);
        panels4.SetActive(false);

    }
    public void SetActiveP4()
    {
        panels1.SetActive(false);
        panels2.SetActive(false);
        panels3.SetActive(false);
        panels4.SetActive(true);

    }
    public void SetActiveP5()
    {
        panels1.SetActive(false);
        panels2.SetActive(false);
        panels3.SetActive(false);
        panels4.SetActive(false);

        Time.timeScale = 1f;
    }
}