﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiSpriteHit : MonoBehaviour {

    public GameObject panels5;



    void Start()
    {
        panels5.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
		
	}
    public void SetActiveP6()
    {
        panels5.SetActive(false);
        Destroy(gameObject);
        Time.timeScale = 1f;
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            Time.timeScale = 0f;
            panels5.SetActive(true);
        }
    }
}
