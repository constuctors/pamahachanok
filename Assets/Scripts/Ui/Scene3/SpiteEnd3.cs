﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpiteEnd3 : MonoBehaviour {

    public GameObject panels1;
    public GameObject panels2;
    public GameObject panels3;
    public int sceneIndex;

    void Start()
    {
        panels1.SetActive(false);
        panels2.SetActive(false);
        panels3.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
		
	}
    public void SetActiveP2()
    {
        panels1.SetActive(false);
        panels2.SetActive(true);
        panels3.SetActive(false);
    }
    public void SetActiveP3()
    {
        panels1.SetActive(false);
        panels2.SetActive(false);
        panels3.SetActive(true);
    }
    public void SetActiveP5()
    {
        panels1.SetActive(false);
        panels2.SetActive(false);
        panels3.SetActive(false);
        Time.timeScale = 1f;
        SceneManager.LoadScene(sceneIndex);
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            Time.timeScale = 0f;
            panels1.SetActive(true);
            panels2.SetActive(false);
            panels3.SetActive(false);
        }
    }
}
