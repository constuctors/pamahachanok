﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiteStart3 : MonoBehaviour {

    public GameObject panels1;
    public GameObject panels2;
    void Start () {
        Time.timeScale = 0f;
        panels1.SetActive(true);
        panels2.SetActive(false);
    }

    public void SetActiveP2()
    {
        panels1.SetActive(false);
        panels2.SetActive(true);


    }
    public void SetActiveP3()
    {
        panels1.SetActive(false);
        panels2.SetActive(false);
        Time.timeScale = 1f;

    }
}
