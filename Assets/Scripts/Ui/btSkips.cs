﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class btSkips : MonoBehaviour {

    private float Ctime = 5;
    public Button btskip;
    public int i;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Ctime -= Time.deltaTime;

        if (Ctime > 0)
        {
            btskip.gameObject.SetActive(false);
        }
        else
        {
            btskip.gameObject.SetActive(true);           
        }
    }
    public void BtSkip()
    {
            SceneManager.LoadScene(i);
    }
}
