﻿using UnityEngine;
using System.Collections;
using System.Data;
using Mono.Data.Sqlite;
using System.Collections.Generic;

public class ConnectDb : MonoBehaviour {
    private string connectionString;

    private List<playerTb> playerTBs = new List<playerTb>();


    void Start ()
    {
        connectionString = "URI=file:" + Application.dataPath + "/GameDb.db";
        //GetData();
        //InputPlayer("dsfs","1230");
    }
	

	void Update () {
	
	}
    private void GetData()
    {
        playerTBs.Clear();

        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();

            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                string sqlQuery = "select * from user";

                dbCmd.CommandText = sqlQuery;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        playerTBs.Add(new playerTb(reader.GetString(1)));
                    }

                    dbConnection.Close();
                    reader.Close();
                }

            }
        }
    }

    private void InputPlayer(string name, Time time)
    {
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            using (IDbCommand dbCmd = dbConnection.CreateCommand())
            {
                string sqlQuery = string.Format("INSERT INTO user (u_name, u_timeP) VALUES (\"{0}\",\"{1}\")", name,time);

                dbCmd.CommandText = sqlQuery;
                dbCmd.ExecuteScalar();
                dbConnection.Close();
            }
        }
    }
}
