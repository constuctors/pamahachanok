﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{

    //push and pull
    public bool frontPlayer = false;
    public bool backPlayer = false;
    public bool L_climbing = false;
    public bool R_climbing = false;
    public float jumpclimb;

    public bool L_pullBox;
    public bool R_pullBox;

    //
    private float move;
    public float p_speed;
    bool stopMoving = false;
    private Rigidbody rb;
    private Animator anim;
    private bool stopFlips;
    bool facingRight;

    //Jumpping
    public bool jump = false;
    bool OnGround = false;
    bool jumping = true;
    public float jumpHight;
    float jumpPressure;

    //Rope climbing
    public bool topPlayer = false;
    public bool upAnddown = false;
    public float ud_speed;
    private float face = 6;

    //Die
    public bool onDie =false;
    float timeCount = 2;
    public bool hitTi = false;
    //
    public bool Nosee = true;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        stopFlips = true;

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (hitTi || onDie)
        {
            timeCount -= Time.deltaTime;
        }

        //Debug.DrawRay(transform.position + new Vector3(0.5f, 3.0f, 0), -transform.up + new Vector3(0.8f, 1, 0), Color.green);
        //Debug.DrawRay(transform.position + new Vector3(-0.5f, 3.0f, 0), -transform.up + new Vector3(-0.8f, 1, 0), Color.red);
        Movement();
        PushAndPull();
        RopeClimp();
        Die();
    }

    void Movement()
    {
        //<---- move
        if (!stopMoving)
        {
            move = Input.GetAxis("Horizontal");
            //anim.SetFloat("running", Mathf.Abs(move));

            rb.velocity = new Vector3(move * p_speed, rb.velocity.y, 0);
            if (move > 0)
            {
                anim.SetFloat("running", move);
                anim.SetBool("isIdle", false);
                if ( facingRight)
                {
                    if (stopFlips == true)
                    {
                        Flip();
                    }

                }
            }
            if (move < 0)
            {
                anim.SetFloat("running", move);
                anim.SetBool("isIdle", false);
                if (!facingRight)
                {
                    if (stopFlips == true)
                    {
                        Flip();
                    }

                }
            }
            if (move == 0)
            {
                anim.SetFloat("running", move);
                anim.SetBool("isIdle", true);
            }
        }
        //<---- jump
        if (jumping)
        {

            if (OnGround)
            {
                jump = true;
                if (jump)
                {
                    if (Input.GetKey(KeyCode.UpArrow))
                    {
                        anim.SetBool("jumping", true);
                        rb.velocity = new Vector3(0, jumpHight, 0.0f);
                        OnGround = false;
                        anim.SetBool("onGround", OnGround);

                    }
                    else
                    {
                        anim.SetBool("jumping", false);
                    }
                }
            }
            else
            {
                jump = false;
                OnGround = false;
                anim.SetBool("onGround", OnGround);


            }
        }
        // ---->
    }

    void Die()
    {
        //----> die
        if (onDie)
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
            stopFlips = false;
            stopMoving = true;
            anim.SetBool("isIdle", false);
            anim.SetFloat("running", 0.0f);
            anim.SetBool("diewater", onDie);
            jumping = false;

            if (timeCount < 1)
            {
                rb.transform.position = CheckPoint.GetActiveCheckPointPosition();
                onDie = false;
                stopFlips = true;
                stopMoving = false;
                anim.SetBool("isIdle", true);
                anim.SetFloat("running", move);
                anim.SetBool("diewater", onDie);
                timeCount = 1;
                jumping = true;
                rb.velocity = new Vector3(move * p_speed, rb.velocity.y, 0);
            }
        }
        if (hitTi)
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
            stopFlips = false;
            stopMoving = true;
            anim.SetBool("isIdle", false);
            anim.SetFloat("running", 0.0f);
            anim.SetBool("dietiger", hitTi);
            jumping = false;

            if (timeCount < 0)
            {
                
                rb.transform.position = CheckPoint.GetActiveCheckPointPosition();
                hitTi = false;
                stopFlips = true;
                stopMoving = false;
                anim.SetBool("isIdle", true);
                anim.SetFloat("running", move);
                anim.SetBool("dietiger", hitTi);
                timeCount = 2;
                jumping = true;
                rb.velocity = new Vector3(move * p_speed, rb.velocity.y, 0);
            }
        }
        
    }


    void PushAndPull()
    {
        RaycastHit LHit;

        //Check hit frontface
        if (Physics.Raycast(transform.position + new Vector3(0.5f, 3.0f, 0), -transform.up + new Vector3(0.8f, 1, 0), out LHit, 0.8f))
        {
            if (LHit.collider.tag == "opject")
            {
                frontPlayer = true;

                if (frontPlayer)
                {
                    L_climbing = true;
                    anim.SetBool("push", true);
                    if (L_climbing)
                    {
                        if (Input.GetKey(KeyCode.UpArrow))
                        {
                            anim.SetBool("L_climbing", L_climbing);
                            jumpclimb = 12;
                            rb.velocity = new Vector3(0, jumpclimb, 0);

                        }
                    }
                }
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Debug.Log("KeyDown");
                    anim.SetBool("pullBox", true);
                    anim.SetBool("push", false);
                    L_pullBox = true;
                    stopFlips = false;
                }
                else if (Input.GetKeyUp(KeyCode.E))
                {
                    Debug.Log("KeyUp");
                    anim.SetBool("pullBox", false);
                    anim.SetBool("push", true);
                    L_pullBox = false;
                    stopFlips = true;
                }
            }
        }
        else
        {
            frontPlayer = false;
            L_climbing = false;
            jumpclimb = jumpHight;
            anim.SetBool("L_climbing", L_climbing);
            anim.SetBool("push", frontPlayer);

        }
        RaycastHit RHit;
        //Check hit backface
        if (Physics.Raycast(transform.position + new Vector3(-0.5f, 3.0f, 0), -transform.up + new Vector3(-0.8f, 1, 0), out RHit, 0.8f))
        {
            if (RHit.collider.tag == "opject")
            {
                backPlayer = true;
                R_climbing = true;
                if (backPlayer)
                {
                    anim.SetBool("pull", true);
                    if (R_climbing)
                    {
                        if (Input.GetKey(KeyCode.UpArrow))
                        {
                            jumpclimb = 12;
                            rb.velocity = new Vector3(0, jumpclimb, 0);
                            anim.SetBool("R_climbing", R_climbing);
                        }
                    }
                }
                if (Input.GetKeyDown(KeyCode.E))
                {
                    anim.SetBool("pullBox", true);
                    anim.SetBool("pull", false);
                    R_pullBox = true;
                    stopFlips = false;
                }
                else if (Input.GetKeyUp(KeyCode.E))
                {
                    anim.SetBool("pullBox", false);
                    anim.SetBool("pull", true);
                    R_pullBox = false;
                    stopFlips = true;
                }

            }
        }
        else
        {
            R_climbing = false;
            jumpclimb = 40;
            backPlayer = false;
            anim.SetBool("R_climbing", R_climbing);
            if (!backPlayer)
            {
                anim.SetBool("pull", backPlayer);
            }
        }
    }

    void RopeClimp()
    {
        if (!OnGround)
        {
            if (topPlayer)
            {
                anim.SetBool("ropeclimb", topPlayer);
                stopMoving = true;
                anim.SetFloat("running", 0.0f);
                rb.velocity = new Vector3(0, rb.velocity.y, 0);
                if (!upAnddown)
                {
                    float ropeclimbing = Input.GetAxis("Vertical");
                    //anim.SetFloat("running", Mathf.Abs(move));
                    rb.velocity = new Vector3(0, ropeclimbing * ud_speed, 0);
                    if (ropeclimbing > 0)
                    {
                        anim.SetBool("climbingrope", true);
                        anim.SetBool("ropeclimb", false);
                       // anim.SetBool("climbingropdown", false);

                    }
                    else if(ropeclimbing < 0)
                    {
                        anim.SetBool("climbingrope", false);
                       // anim.SetBool("climbingropdown", true);
                        anim.SetBool("ropeclimb", false);
                    }
                    else
                    {
                        anim.SetBool("climbingrope", false);
                        //anim.SetBool("climbingropdown", false);
                        anim.SetBool("ropeclimb", true);
                    }
                }
            }
        }
        if (OnGround)
        {
            stopMoving = false;
            topPlayer = false;
            anim.SetBool("ropeclimb", topPlayer);
            anim.SetBool("climbingrope", false);
        }
    }



    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.z *= -1;
        transform.localScale = theScale;
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "tiger")
        {
            hitTi = true;
        }
        if (other.gameObject.tag == "solder")
        {
            hitTi = true;
        }

        if (other.gameObject.CompareTag("ground")|| other.gameObject.CompareTag("opject"))
        {
            OnGround = true;

            anim.SetBool("onGround", OnGround);
        }
        if (other.gameObject.CompareTag("trap"))
        {
            onDie = true;
        }
        if (other.gameObject.CompareTag("rebound"))
        {
            
            if (facingRight)
            {
                face *= -1;
            }
            
            transform.Translate(0, 0, face);
            topPlayer = false;
            anim.SetBool("ropeclimb", topPlayer);
            anim.SetBool("climbingrope", false);
        }
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("rope"))
        {
            transform.position = gameObject.transform.position + Vector3.up * 1 + Vector3.right *0.4f;
            topPlayer = true;
        }
                if (!col.gameObject.CompareTag("rope"))
        {
            topPlayer = false;
            anim.SetBool("ropeclimb", topPlayer);
            anim.SetBool("climbingrope", false);
        }
        if (col.CompareTag("NoSee"))
        {
            Nosee = true;
        }
        if (col.gameObject.CompareTag("trap"))
        {
            onDie = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.CompareTag("rope"))
        {
            topPlayer = false;
        }
    }

}