﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pullOpject : MonoBehaviour {

    public Transform player;
    public Player pull;
    public bool L_pullTrue;
    public bool R_pullTrue;
    private Vector3 direction;
    public float bf;

    void Start()
    {

    }
    // Use this for initialization
    void FixedUpdate()
    {
        L_pullTrue = pull.L_pullBox;
        R_pullTrue = pull.R_pullBox;
        direction = player.position - this.transform.position;
        if (L_pullTrue)
        {
            if (direction.magnitude > bf)
            {
                this.transform.Translate(-0.17f, 0, 0);
            }
        }
        if (R_pullTrue)
        {
            if (direction.magnitude > bf)
            {
                this.transform.Translate(0.17f, 0, 0);
            }
        }
    }
}
