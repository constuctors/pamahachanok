﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class pauseGame : MonoBehaviour {
    public bool paused = false;
    public GameObject panelsMenu;


    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = togglePause();
            panelsMenu.SetActive(!panelsMenu.activeSelf);
        }   

    }


    bool togglePause()
    {
        if (Time.timeScale == 0f)
        {
            Time.timeScale = 1f;
            return (false);
        }
        else
        {
            Time.timeScale = 0f;
            return (true);
        }
    }
    public void TogglePanel(GameObject panel)
    {
            paused = togglePause();
            panel.SetActive(!panel.activeSelf);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1f;
    }

}
