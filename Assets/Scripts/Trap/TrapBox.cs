﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapBox : MonoBehaviour {

    public GameObject boxtrap;
    public GameObject setPosition;
    public Player playDie;
	void Start () {
        boxtrap.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (playDie.onDie)
        {
            boxtrap.transform.position = setPosition.transform.position;
            boxtrap.SetActive(false);
        }
	}
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            boxtrap.SetActive(true);
        }
    }
}
