﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class AudioSound : MonoBehaviour {

    public AudioMixerSnapshot outOfCombat;
    public AudioMixerSnapshot inCombat;
    public AudioMixerSnapshot inCombatWater;
    public AudioClip[] strings;
    public AudioSound stringSource;
    public float bpm = 100;

    private float m_TransitionIn;
    private float m_TransitionOut;
    private float m_QuarterNote;

	void Start () {

        m_QuarterNote = 60 / bpm;
        m_TransitionIn = m_QuarterNote;
        m_TransitionOut = m_QuarterNote * 20;
	}
	
	void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ActionZone"))
        {
            inCombat.TransitionTo(m_TransitionIn);
        }

        if (other.CompareTag("WaterZone"))
        {
            inCombatWater.TransitionTo(m_TransitionIn);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("ActionZone"))
        {
            outOfCombat.TransitionTo(m_TransitionOut);
        }

        if (other.CompareTag("WaterZone"))
        {
            outOfCombat.TransitionTo(m_TransitionOut);
        }
    }
}
