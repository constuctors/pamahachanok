﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]

public class playVideo : MonoBehaviour {


    public MovieTexture movie;
    private new AudioSource audio;
    public int sceneIndex;


    void Start () {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        audio = GetComponent<AudioSource>();
        audio.clip = movie.audioClip;
        movie.Play();
        audio.Play(); 
	}

    void Update()
    {
        
        if (!movie.isPlaying)
        {
            SceneManager.LoadScene(sceneIndex);
        }
    }
}
