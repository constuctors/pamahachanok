using UnityEngine;
using System.Collections;

public class contro_Player : MonoBehaviour
{

    //die
    private bool onDieying;
    private bool onTigerAttack;

    //Jumpping
    public bool jump = false;
    bool OnGround = false;
    bool jumping  = true;
	Collider[] groundCollisions;
	float groundCheckRadius = 0.2f;
	public LayerMask groundLayer;
	public Transform groundCheck;
	public float jumpHight;
    float jumpPressure;

    //movement
    public float p_speed;
    bool stopMoving = false;
    private Rigidbody rb;
    private Animator anim;
    private bool stopFlips;
    bool facingRight;


    //push and pull
    public bool frontPlayer = false;
    public bool backPlayer = false;
    public bool climbing = false;
    //Rope climbing
    public bool topPlayer = false;
    public bool upAnddown = false;
    public float ud_speed;



    // Use this for initialization
    void Start()
    {
        stopFlips = true;       
        onDieying = false;
        onTigerAttack = false;

        //OnGround = true;

        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        




    }

    void Update()
    {
        
    }
    void FixedUpdate()
    {
        Debug.DrawRay(transform.position+ new Vector3(0.5f, 3.0f, 0), -transform.up + new Vector3(0.8f, 1, 0), Color.green);
        Debug.DrawRay(transform.position + new Vector3(-0.5f, 3.0f, 0), -transform.up + new Vector3(-0.8f, 1, 0), Color.red);
        Movement();
        PushAndPull();
        RopeClimp();

    }


    void Movement()
    {
        //<---- jump
        if (jumping)
        {
            groundCollisions = Physics.OverlapSphere(groundCheck.position, groundCheckRadius, groundLayer);
            jumpPressure += Time.deltaTime * 10f;
            if (groundCollisions.Length > 0)
            {
                OnGround = true;
                anim.SetBool("onGround", OnGround);
                jump = true;
            }
            else
            {
                jump = false;
                OnGround = false;
                anim.SetBool("onGround", OnGround);
                anim.SetBool("jump", jump);
            }
            if (OnGround)
            {               
                if (Input.GetButton("Jump"))
                {
                    jumpPressure = jumpPressure + jumpHight;
                    rb.velocity = new Vector3(jumpPressure / 10f, jumpHight, 0.0f);
                    OnGround = false;
                    anim.SetBool("onGround", OnGround);
                    anim.SetBool("jump", jump);
                    
                }

            }
        }

        //<---- move
        if(!stopMoving)
        {
            float move = Input.GetAxis("Horizontal");
            //anim.SetFloat("running", Mathf.Abs(move));

            rb.velocity = new Vector3(move * p_speed, rb.velocity.y, 0);
            if (move != 0)
            {
                anim.SetBool("Running", true);
                anim.SetBool("isIdle", false);

                if (move > 0 && facingRight)
                {
                    if (stopFlips == true)
                    {
                        Flip();
                    }

                }

                else if (move < 0 && !facingRight)
                {
                    if (stopFlips == true)
                    {
                        Flip();
                    }
                }
            }
            else
            {
                anim.SetBool("Running", false);
                anim.SetBool("isIdle", true);
            }
        }


        // ---->


        
    }
    void Die()
    {
        //----> die
        if (onDieying)
        {
            stopFlips = false;
            stopMoving = true;
            anim.SetBool("isIdle", false);
            anim.SetBool("Running", false);
            anim.SetBool("die", onDieying);
        }
        if (onTigerAttack)
        {

            rb.velocity = new Vector3(0, 0, 0);
            anim.SetBool("isIdle", false);
            anim.SetBool("Running", false);
            anim.SetBool("die", onDieying);
        }
    }



    void PushAndPull()
    {
        RaycastHit LHit;
        RaycastHit RHit;
        //Check hit frontface
        if (Physics.Raycast(transform.position + new Vector3(0.5f, 3.0f, 0), -transform.up + new Vector3(0.8f, 1, 0), out LHit, 1f))
        {
            if (LHit.collider.tag == "opject")
            {
                frontPlayer = true;
                climbing = true;
                if (frontPlayer)
                {
                    anim.SetBool("push", frontPlayer);
                    if (climbing)
                    {
                        if (Input.GetKey(KeyCode.UpArrow))
                        {
                            jumpHight = 12;
                            rb.velocity = new Vector3(0, jumpHight, 0);
                            anim.SetBool("climbing", climbing);
                        }
                    }
                }
                else
                {
                    climbing = false;
                    jumpHight = 10;
                    frontPlayer = false;
                    anim.SetBool("push", frontPlayer);
                    anim.SetBool("climbing", climbing);
                }
            }                
        }
        

        //Check hit backface
        if (Physics.Raycast(transform.position + new Vector3(-0.5f, 3.0f, 0), -transform.up + new Vector3(-0.8f, 1, 0), out RHit, 1f))
        {
            backPlayer = true;
            if (backPlayer)
            {
                anim.SetBool("push", backPlayer);
            }
        }
        else
        {

            backPlayer = false;
            if (backPlayer)
            {
                anim.SetBool("push", backPlayer);
            }
        }
    }

    void RopeClimp()
    {
        if (!OnGround)
        {
            if (topPlayer)
            {
                anim.SetBool("idleRope", topPlayer);
                stopMoving = true;
                rb.velocity = new Vector3(0, rb.velocity.y, 0);
                if (!upAnddown)
                {
                    float move = Input.GetAxis("Vertical");
                    //anim.SetFloat("running", Mathf.Abs(move));

                    rb.velocity = new Vector3(0, move * ud_speed, 0);
                    if (move != 0)
                    {
                        anim.SetBool("Ropeclimbing", true);
                        anim.SetBool("idleRope", false);

                    }
                    else
                    {
                        anim.SetBool("Ropeclimbing", false);
                        anim.SetBool("idleRope", true);
                    }
                }
            }
        }
        if (OnGround)
        {
            stopMoving = false;
            topPlayer = false;
            anim.SetBool("idleRope", topPlayer);
            anim.SetBool("Ropeclimbing", false);
        }
        
    }


    void Flip()
    {        
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.z *= -1;
        transform.localScale = theScale;
    }



    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("ground"))
        {

            OnGround = true;
            anim.SetBool("onGround", OnGround);
        }
        if (other.gameObject.CompareTag("trap"))
        {
            onDieying = true;
        }

        if (other.gameObject.CompareTag("tiger"))
        {
            onTigerAttack = true;
        }
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("rope"))
        {
            transform.position = gameObject.transform.position + Vector3.up *1;
            topPlayer = true;
        }
        if (!col.gameObject.CompareTag("rope"))
        {
            topPlayer = false;
            transform.position = gameObject.transform.position + Vector3.down *0.4f;
        }
    }

}
