﻿using UnityEngine;
using System.Collections;

public class Solderkill : MonoBehaviour
{

    public Transform player;
    private Rigidbody botrb;
    private Animator anim;
    private Vector3 direction;
    public PlayerScene2 die;
    public bool killP = false;
    public bool donMove = false;
    public float bf;
    float tm = 2;


    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        botrb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (killP || die.onDie)
        {
            tm -= Time.deltaTime;
        }
        if (die.onDie)
        {
            tm = 2;
            killP = false;
            botrb.transform.position = checkpointbot.GetActiveCheckPointPosition();
            donMove = false;
            anim.SetBool("idle", true);
            anim.SetBool("attack", false);
            anim.SetBool("run", false);
        }
        
        
            direction = player.position - this.transform.position;
            if (Vector3.Distance(player.position, this.transform.position) < 40)
            {
                direction.y = 0;
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 1);

                anim.SetBool("idle", false);
                if (direction.magnitude > bf)
                {
                    if (!donMove)
                {
                    if (!killP)
                    {
                        this.transform.Translate(0, 0, 0.16f);
                        anim.SetBool("run", true);
                        anim.SetBool("attack", false);
                        anim.SetBool("idle", false);
                    }
                }
                else if (direction.magnitude == bf)
                {
                    anim.SetBool("run", false);
                    anim.SetBool("attack", true);
                    anim.SetBool("idle", false);
                }
            }
                else
                {
                    anim.SetBool("idle", true);
                    anim.SetBool("attack", false);
                    anim.SetBool("run", false);
                }
               
            }
            else
            {
                anim.SetBool("idle", true);
                anim.SetBool("attack", false);
                anim.SetBool("run", false);
            }
            KillS();
        
        
            
    }
    void KillS()
    {
        if (killP)
        {
            anim.SetBool("idle", false);
            anim.SetBool("attack", true);
            anim.SetBool("run", false);
            if (tm < 0)
            {
                tm = 2;
                killP = false;
                botrb.transform.position = checkpointbot.GetActiveCheckPointPosition();
                donMove = false;
                anim.SetBool("idle", true);
                anim.SetBool("attack", false);
                anim.SetBool("run", false);
            }
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            killP = true;
        }
        if (collision.gameObject.tag == "trap")
        {
            killP = false;
            botrb.transform.position = checkpointbot.GetActiveCheckPointPosition();
            donMove = false;
            anim.SetBool("idle", true);
            anim.SetBool("attack", false);
            anim.SetBool("run", false);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "DontMove")
        {
            donMove = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "DontMove")
        {
            donMove = false;
        }
    }
}
