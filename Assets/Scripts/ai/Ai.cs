﻿using UnityEngine;
using System.Collections;

public class Ai : MonoBehaviour
{
    public Transform Ai_Player;
    public Transform[] seobj;
    private Animator an;
    public float speed = 5.0f;
    public float reachDist = 1.0f;
    public int currentPoint = 0;
    public float bf;
    public float countNum = 5;
    public float i_coutNum;

    public bool move = false;
    void Start()
    {
        an = GetComponent<Animator>();
    }


    void Update()
    {
        countNum -= Time.deltaTime;


        float dist = Vector3.Distance(seobj[currentPoint].position, transform.position);
        Vector3 direction = seobj[currentPoint].position - this.transform.position;
        Quaternion vb = Quaternion.LookRotation(direction);
        if (Vector3.Distance(seobj[currentPoint].position, this.transform.position) < 25)
        {
            direction.y = 0;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, vb, 1);

            an.SetBool("idle", false);
            if (direction.magnitude > bf)
            {
                this.transform.position = Vector3.Slerp(transform.position, seobj[currentPoint].position, Time.deltaTime * speed);
                an.SetBool("run", true);
                an.SetBool("idle", false);

            }
            else
            {
                an.SetBool("idle", true);
                an.SetBool("run", false);
                if (countNum <= 0)
                {
                    if (currentPoint <= reachDist)
                    {
                        currentPoint++;
                    }
                    if (currentPoint >= seobj.Length)
                    {
                        currentPoint = 0;
                    }
                    countNum = i_coutNum;
                }

            }
            

        }
    }
}
