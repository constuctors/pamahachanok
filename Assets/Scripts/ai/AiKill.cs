﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiKill : MonoBehaviour {

	 public Transform Ai_Player;
    public Transform[] seobj;
    private Animator an;
    public float speed = 3.0f;
    public float reachDist = 1.0f;
    public int currentPoint = 0;
    float countNum = 5;
    public float bf;
    public PlayerScene2 nosee;
    public float i_coutNum;
    public bool see;
    
    public bool move = false;
    void Start()
    {
        an = GetComponent<Animator>();
    }


    void Update()
    {
        see = nosee.Nosee;
        MovetoPlayer();
        if (!move)
        {
            LookFor();
        }
    }
    void LookFor()
    {
        countNum -= Time.deltaTime;


        float dist = Vector3.Distance(seobj[currentPoint].position, transform.position);
        Vector3 direction = seobj[currentPoint].position - this.transform.position;
        Quaternion vb = Quaternion.LookRotation(direction);
        if (Vector3.Distance(seobj[currentPoint].position, this.transform.position) < 25)
        {
            direction.y = 0;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, vb, 1);

            an.SetBool("idle", false);
            if (direction.magnitude > bf)
            {
                this.transform.position = Vector3.Slerp(transform.position, seobj[currentPoint].position, Time.deltaTime * speed);
                an.SetBool("run", true);
                an.SetBool("idle", false);

            }
            else
            {
                an.SetBool("idle", true);
                an.SetBool("run", false);
                if (countNum <= 0)
                {
                    if (currentPoint <= reachDist)
                    {
                        currentPoint++;
                    }
                    if (currentPoint >= seobj.Length)
                    {
                        currentPoint = 0;
                    }
                    countNum = i_coutNum;
                }

            }
        }
    }

    void MovetoPlayer()
    {
        if (!see)
        {
            Vector3 direction = Ai_Player.position - this.transform.position;
            float angle = Vector3.Angle(direction, this.transform.forward);
            if (Vector3.Distance(Ai_Player.position, this.transform.position) < 40 && angle < 45)
            {

                move = true;
                an.SetBool("idle", false);
                an.SetBool("Attack", true);
                an.SetBool("run", false);
                /*
                direction.y = 0;

                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);
                if (direction.magnitude > 10)
                {
                    this.transform.Translate(0, 0, 0.05f);
                }*/

            }
            else
            {
                move = false;
                an.SetBool("Attack", false);
            }
        }

    }
}
