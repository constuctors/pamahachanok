﻿using UnityEngine;
using System.Collections;

public class SoderSee : MonoBehaviour {

    public Transform Ai_Player;
    public Transform[] seobj;
    public float speed = 3.0f;
    public float reachDist = 1.0f;
    public int currentPoint = 0;
    float countNum = 4;
    public Player nosee;
    public bool see;
    public bool move;
	void Start ()
    {
        
	}
	
	
	void Update ()
    {
        see = nosee.Nosee;
        MovetoPlayer();
        if (!move)
        {
            LookFor();
        }
    }
    void LookFor()
    {
        countNum -= Time.deltaTime;

        float dist = Vector3.Distance(seobj[currentPoint].position, transform.position);
        Vector3 direct = seobj[currentPoint].position - this.transform.position;
        transform.position = Vector3.Lerp(transform.position, seobj[currentPoint].position, Time.deltaTime * speed);
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direct), 0.1f);

        if (countNum <= 0)
        {
            if (dist <= reachDist)
            {
                currentPoint++;
            }
            if (currentPoint >= seobj.Length)
            {
                currentPoint = 0;
            }
            countNum = 4;
        }
    }

    void MovetoPlayer()
    {
        if (see)
        {
            Vector3 direction = Ai_Player.position - this.transform.position;
            float angle = Vector3.Angle(direction, this.transform.forward);
            if (Vector3.Distance(Ai_Player.position, this.transform.position) < 10 && angle < 30)
            {

                move = true;
                direction.y = 0;

                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);
                if (direction.magnitude > 10)
                {
                    this.transform.Translate(0, 0, 0.05f);
                }

            }
            else
            {
                move = false;
            }
        }
        
    }



    /*
    public Transform Ai_Player;
    float rotateFace = 4;
    public float num = 0;
    //static Animator am;
	void Start ()
    {
        //am = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        rotateFace -= Time.deltaTime;
        if (rotateFace <= 0)
        {
            num += Time.deltaTime;
            this.transform.Rotate(new Vector3(0, 90, 0) * num);
            if (num > 0.2)
            {
                rotateFace = 4;
                num = 0;
            }


        }

        Vector3 direction = Ai_Player.position - this.transform.position;
        float angle = Vector3.Angle(direction, this.transform.forward);
        if (Vector3.Distance(Ai_Player.position, this.transform.position) < 10 && angle<30)
        {
            Debug.Log("See You");

            direction.y = 0;

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);
            if (direction.magnitude > 0.1)
            {
                this.transform.Translate(0, 0, 0.05f);
            }

        }

	
	}
    */
}
