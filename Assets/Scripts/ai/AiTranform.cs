﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiTranform : MonoBehaviour {

    public GameObject opj1;
    public GameObject opj2;
    public bool hitGo;
    public float sp_Ai =0,dura = 2;
    private Vector3 b1;
    private Vector3 b2;
    public AudioSource ads;

	void Start () {
        b1 = opj1.transform.position;
        b2 = opj2.transform.position;

    }
    void LateUpdate()
    {
        if (hitGo)
        {
            sp_Ai += Time.deltaTime / dura;            
            opj2.transform.position = Vector3.Lerp(b2, b1, sp_Ai);
        }
        
        
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            hitGo = true;
            ads.Play();
            
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            hitGo = false;
            Destroy(gameObject);

        }
    }
}
