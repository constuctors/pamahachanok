﻿using UnityEngine;
using System.Collections;

public class Bot : MonoBehaviour {

    public Transform player;
    private Rigidbody botrb;
    private Animator anim;
    public bool killP = false;
    float tm =2;


	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        botrb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (killP)
        {
            tm -= Time.deltaTime;
        }
        
            Vector3 direction = player.position - this.transform.position;
            if (Vector3.Distance(player.position, this.transform.position) < 45)
            {
                direction.y = 0;
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 1);

                anim.SetBool("isIdle", false);
                if (direction.magnitude > 5)
                {
                    if (!killP)
                    {
                        this.transform.Translate(0, 0, 0.15f);
                        anim.SetBool("isWalking", true);
                        anim.SetBool("isIdle", false);
                    }
                }
            }
            else
            {
                anim.SetBool("isIdle", true);
                anim.SetBool("isAttack", false);
                anim.SetBool("isWalking", false);
            }
            KillS();
        
        
	}
    void KillS()
    {
        if (killP)
        {
            anim.SetBool("isIdle", false);
            anim.SetBool("isAttack", true);
            anim.SetBool("isWalking", false);
            if (tm < 0)
            {
                tm = 2;
                killP = false;
                botrb.transform.position = checkpointbot.GetActiveCheckPointPosition();
                transform.rotation = Quaternion.identity;
                
                anim.SetBool("isIdle", true);
                anim.SetBool("isAttack", false);
                anim.SetBool("isWalking", false);
            }
        }
        }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            killP = true;
        }
    }

}
