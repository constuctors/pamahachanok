﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolderTorch : MonoBehaviour {



    public Transform player;
    private Rigidbody botrb;
    private Animator anim;
    private Vector3 direction;
    public Solderkill killp;
    public bool donMove = false;
    public PlayerScene2 die;
    public float bf;
    float tm = 2;


    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        botrb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (killp.killP || die.onDie)
        {
            tm -= Time.deltaTime;
        }
        if (die.onDie)
        {
            tm = 2;
            botrb.transform.position = checkpointsolder.GetActiveCheckPointPosition();
            donMove = false;
            anim.SetBool("idle", true);
            anim.SetBool("run", false);
        }

        direction = player.position - this.transform.position;
            if (Vector3.Distance(player.position, this.transform.position) < 45)
            {
                direction.y = 0;
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 1);

                anim.SetBool("idle", false);
                if (direction.magnitude > bf)
                {
                    if (!donMove)
                    {
                        if (!killp.killP)
                        {
                            this.transform.Translate(0, 0, 0.15f);
                            anim.SetBool("run", true);
                            anim.SetBool("idle", false);
                        }
                    }
                    else if (direction.magnitude == bf)
                    {
                        anim.SetBool("run", false);
                        
                        anim.SetBool("idle", true);
                    }
                }
                else
                {

                    anim.SetBool("run", false);
                    anim.SetBool("idle", true);
                }
            }
            else
            {

                anim.SetBool("idle", true);
                anim.SetBool("run", false);
            }
            KillS();       
        
    }
    void KillS()
    {
        if (killp.killP)
        {
            anim.SetBool("idle", true);
            anim.SetBool("run", false);
            if (tm < 0)
            {
                tm = 2;               
                botrb.transform.position = checkpointsolder.GetActiveCheckPointPosition();
                donMove = false;
                anim.SetBool("idle", true);
                anim.SetBool("run", false);
            }
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            KillS();
        }
        if (collision.gameObject.tag == "trap")
        {
            KillS();
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "DontMove")
        {
            donMove = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "DontMove")
        {
            donMove = false;
        }
    }

}
