﻿using UnityEngine;

public class checkpointbot : MonoBehaviour
{
    #region Public Variables

    /// <summary>
    /// Indicate if the checkpoint is activated
    /// </summary>
    public bool Activated = false;

    #endregion

    #region Private Variables


    #endregion

    #region Static Variables

    /// <summary>
    /// List with all checkpoints objects in the scene
    /// </summary>
    public static GameObject[] CheckPointsList;

    #endregion

    #region Static Functions

    /// <summary>
    /// Get position of the last activated checkpoint
    /// </summary>
    /// <returns></returns>
    public static Vector3 GetActiveCheckPointPosition()
    {
        // If player die without activate any checkpoint, we will return a default position
        Vector3 result = new Vector3(0, 0, 0);

        if (CheckPointsList != null)
        {
            foreach (GameObject cp in CheckPointsList)
            {
                // We search the activated checkpoint to get its position
                if (cp.GetComponent<checkpointbot>().Activated)
                {

                    cp.transform.Rotate(0, 0, 0);
                    result = cp.transform.position;
                    break;
                }
            }
        }

        return result;
    }

    #endregion

    #region Private Functions

    /// <summary>
    /// Activate the checkpoint
    /// </summary>
    private void ActivateCheckPoint()
    {
        // We deactive all checkpoints in the scene
        foreach (GameObject cop in CheckPointsList)
        {
            cop.GetComponent<checkpointbot>().Activated = false;
        }

        // We activated the current checkpoint
        Activated = true;
    }

    #endregion

    void Start()
    {


        // We search all the checkpoints in the current scene
        CheckPointsList = GameObject.FindGameObjectsWithTag("CheckPointBot");
    }

    void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "tiger")
        {
            ActivateCheckPoint();
        }
        if(other.tag == "solder")
        {
            ActivateCheckPoint();
        }
    }
}

