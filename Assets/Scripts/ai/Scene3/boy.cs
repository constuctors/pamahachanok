﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boy : MonoBehaviour
{

    public GameObject opj1;
    public GameObject opj2;
    public Spitehit hitplayer;
    public bool hitGo;
    public float sp_Ai = 0, dura = 2;
    private Vector3 b1;
    private Vector3 b2;
    private Animator ant;

    void Start()
    {
        ant = GetComponent<Animator>();
        b1 = opj1.transform.position;
        b2 = opj2.transform.position;

    }
    void LateUpdate()
    {
        hitGo = hitplayer.goOn;
        if (hitGo)
        {
            sp_Ai += Time.deltaTime / dura;
            opj2.transform.position = Vector3.Lerp(b2, b1, sp_Ai);
            if(opj2.transform.position != opj1.transform.position)
            {
                ant.SetBool("idle", false);
                ant.SetBool("run", true);
            }
            else
            {
                ant.SetBool("idle", true);
                ant.SetBool("run", false);
            }
        }

       


    }
}
