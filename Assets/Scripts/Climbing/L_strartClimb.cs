﻿using UnityEngine;
using System.Collections;

public class L_strartClimb : MonoBehaviour {

    public L_Climb climb;

	// Use this for initialization
	void OnTriggerEnter(Collider cl)
    {
        if (cl.gameObject.CompareTag("Player"))
        {          
            climb.L_climbing = true;
            climb.L_nextPos = false;
        }
    }
    void OnTriggerExit(Collider cl)
    {
        climb.L_climbing = false;
    }
}
