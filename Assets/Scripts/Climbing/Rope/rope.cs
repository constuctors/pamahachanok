﻿using UnityEngine;
using System.Collections;

public class rope : MonoBehaviour {

    public GameObject plantRope;
    public bool checkrope = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (checkrope)
        {
            plantRope.gameObject.SetActive(true);

        }
        else
        {
            plantRope.gameObject.SetActive(false);
        }

    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            checkrope = true;
        }
    }
}
