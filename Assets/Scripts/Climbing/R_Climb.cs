﻿using UnityEngine;
using System.Collections;

public class R_Climb : MonoBehaviour
{
    public GameObject rightOpj;
    public Transform R_movingPlatform;
    public Transform R_position1;
    public Transform R_position2;
    public Transform R_position3;

    private Vector3 newPosition;
    public float smooth;
    public bool R_climbing = false;
    public bool R_nextPos = false;
    public bool R_checkHit = false;
    // Use this for initialization
    void Start()
    {

        //ChangeTarget();
    }

    void FixedUpdate()
    {               
            if (R_climbing && !R_nextPos)
                {
                    if(Input.GetKey(KeyCode.E))
                    {
                        newPosition = R_position2.position;
                    }


                }
                if (R_nextPos && !R_climbing)
                {
                    newPosition = R_position3.position;

                }
                else
                if (!R_climbing && !R_nextPos)
                {
                    newPosition = R_position1.position;
                    smooth = 1;
                }
                R_movingPlatform.position = Vector3.Lerp(R_movingPlatform.position, newPosition, smooth* Time.deltaTime);  
                
    }
    /*void CheckHit()
    {

        if (R_checkHit)
        {
            rightOpj.SetActive(true);
        }

        if (!R_checkHit)
        {
            rightOpj.SetActive(false);
        }
    }*/



}
