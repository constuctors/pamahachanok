﻿using UnityEngine;
using System.Collections;

public class endClimb : MonoBehaviour {

    
    public L_Climb climbL;
    public R_Climb climbR;
    // Use this for initialization
    void OnTriggerEnter(Collider cl)
    {
        if (cl.gameObject.CompareTag("Player"))
        {
            climbL.L_climbing = false;
            climbL.L_nextPos = false;
            climbR.R_climbing = false;
            climbR.R_nextPos = false;

        }
    }
}
