﻿using UnityEngine;
using System.Collections;

public class L_nextClimb : MonoBehaviour {

    public L_Climb climb;
	// Use this for initialization
	void OnTriggerEnter(Collider cl) 
    {
        if (cl.gameObject.CompareTag("Player"))
        {
            climb.L_climbing = false;
            climb.L_nextPos = true;
        }
    }
}
