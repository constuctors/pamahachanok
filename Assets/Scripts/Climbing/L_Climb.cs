﻿using UnityEngine;
using System.Collections;

public class L_Climb : MonoBehaviour
{
    public GameObject LeftOpj;
    public Transform L_movingPlatform;
    public Transform L_position1;
    public Transform L_position2;
    public Transform L_position3;
    
    private Vector3 newPosition;
    public float smooth;
    public bool L_checkHit = false;
    public bool L_climbing = false;
    public bool L_nextPos = false;

    // Use this for initialization
    void Start()
    {

        //ChangeTarget();
    }

    void FixedUpdate()
    {
        //CheckHit();
        //Debug.DrawRay(transform.position + new Vector3(-1,0, 0), -transform.up + new Vector3(-3, 1, 0), Color.red);

        if (L_climbing && !L_nextPos)
        {
            if (Input.GetKey(KeyCode.E))
            {
                newPosition = L_position2.position;
                smooth = 0.5f;
            }


        }
        if (L_nextPos && !L_climbing)
        {
            newPosition = L_position3.position;

        }
        else
        if (!L_climbing && !L_nextPos)
        {
            newPosition = L_position1.position;
            smooth = 1;
        }
        L_movingPlatform.position = Vector3.Lerp(L_movingPlatform.position, newPosition, smooth * Time.deltaTime);
    }
    /*void CheckHit()
    {

        if (L_checkHit)
        {
            LeftOpj.SetActive(true);
        }

        if (!L_checkHit)
        {
            LeftOpj.SetActive(false);
        }
    }*/


}
