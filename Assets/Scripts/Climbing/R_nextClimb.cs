﻿using UnityEngine;
using System.Collections;

public class R_nextClimb : MonoBehaviour {

    public R_Climb climb;
	// Use this for initialization
	void OnTriggerEnter(Collider cl) 
    {
        if (cl.gameObject.CompareTag("Player"))
        {
            climb.R_climbing = false;
            climb.R_nextPos = true;
        }
    }
}
