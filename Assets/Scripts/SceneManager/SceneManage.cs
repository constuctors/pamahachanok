﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneManage : MonoBehaviour {

    public int sceneIndex;
    // Use this for initialization
    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Player"))
        {
            SceneManager.LoadScene(sceneIndex);
        }
    }
}
